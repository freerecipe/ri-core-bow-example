package info.atmykitchen.ricore.example;



import java.util.Iterator;
import java.util.TreeMap;

/**
 * 
 * @author Behrang QasemiZadeh <me at atmykitchen.info>
 */
public class SparseIntegerVector extends TreeMap<Integer, Integer> {

    /**
     *
     */
    private int diemension = 0;
    private boolean convertedToArray;
    private int[] keySetArray;
    private int[] valueSet;
    double lengthPower2;
    double lengthPower4;
    boolean lengthCompuetd;

    /**
     * create empty vector
     */
    public SparseIntegerVector() {
        super();
        lengthCompuetd = false;
        convertedToArray = false;
    }

    /**
     * create empty vector with length
     */
    public SparseIntegerVector(int i) {
        this();
        diemension = i;
        lengthCompuetd = false;
        convertedToArray = false;
    }

   
    /**
     * copy constructor
     *
     * @param v
     */
    public SparseIntegerVector(SparseIntegerVector v) {
        super(v);
        this.diemension = v.diemension;
        lengthCompuetd = false;
        convertedToArray = false;
    }

    /**
     * get ensures it returns 0 for empty hash values or if index exceeds
     * length.
     *
     * @param key
     * @return val
     */
    @Override
    public Integer get(Object key) {
        Integer b = super.get(key);
        if (b == null) {
            return 0;
        }
        return b;
    }

   

    public void incValue(Integer key, Integer value) {
        Integer c = get(key);
        c += value;
        put(key, c);
    }

  

    /**
     * sum of the elements
     *
     * @return
     */
    public double sum() {
        double sum = 0;
        for (double a : values()) {
            sum += a;
        }
        return sum;
    }

    public void multiplyByConstant(int constantFreq) {
        for (int i : this.keySet()) {
            mult(i, constantFreq);
        }
    }

    /**
     * add vector to another vector
     *
     * @param v
     */
    public void addVector(SparseIntegerVector v) {
        for (int i : v.keySet()) {
            Integer elementValue = this.get(i) + v.get(i);
            this.put(i, elementValue);
        }
    }

    public void addMultiplyVector(SparseIntegerVector v, int mult) {
        for (int i : v.keySet()) {
            Integer elementValue = this.get(i) + (v.get(i) * mult);
            this.put(i, elementValue);
        }
    }

    public void addVectorPermuted(SparseIntegerVector v, int permutationOffset) {
        for (int index : v.keySet()) {
            int newPermutedOffset = permutationOffset + index;
            if (newPermutedOffset < this.diemension && newPermutedOffset >= 0) {
                Integer elementValue = this.get(newPermutedOffset) + v.get(index);
                this.put(index, elementValue);
            } else {
                // I ignor the rest of elements in this permutation, I believe it is not important, at least 
                // for the time that I have this very large beta values -- maybe add new permutation later
            }
        }
    }

    /**
     * mutable mult
     *
     * @param i index
     * @param a value
     */
    public void mult(int i, int a) {
        Integer c = get(i);
        c *= a;
        put(i, c);
    }

    

   

  
    /**
     * get the length of the vector
     *
     * @return
     */
    public final int getDimension() {
        return diemension;
    }

    public int[] getKeySetArray() {
        return keySetArray;
    }

    public int[] getValueSet() {
        return valueSet;
    }

    /**
     * set the new length of the vector (regardless of the maximum index).
     *
     * @param length
     */
    public final void setLength(int length) {
        this.diemension = length;
    }

   

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i : keySet()) {
            sb.append(i).append(":").append(get(i).toString()).append(" ");
        }
        return sb.toString();
    }

    public String toStringInteger() {
        StringBuffer sb = new StringBuffer();
        for (int i : keySet()) {
            sb.append(i).append(":").append(get(i).intValue()).append(" ");
        }
        return sb.toString();
    }

    public String toStringPresentation() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < diemension; i++) {
            if (keySet().contains(i)) {
                sb.append(get(i).toString().substring(0, 1) + ",");
            } else {
                sb.append("0,");
            }

        }

        return sb.toString();
    }

    public void fromString(String line) {

        String[] splitLine = line.split(" ");
        for (int i = 0; i < splitLine.length; i++) {
            String[] splitBit = splitLine[i].split(":");
            int index = Integer.parseInt(splitBit[0]);
            int value = Integer.parseInt(splitBit[1]);
            this.put(index, value);
        }

        //return sb.toString();
    }

    

    public boolean isConvertedToArray() {
        return convertedToArray;
    }

    public void convertToArrays() {

        keySetArray = new int[keySet().size()];
        valueSet = new int[keySet().size()];
        Iterator<Integer> iterator = keySet().iterator();
        int counter = 0;
        while (iterator.hasNext()) {
            keySetArray[counter] = iterator.next();
            valueSet[counter] = this.get(keySetArray[counter]);
            counter++;
        }
        convertedToArray = true;
    }

  

   

  

    /**
     * 
     * @param v
     * @return 
     */
    public double getCosine(SparseIntegerVector v) {
        double cosine = 0.0;
        double length1 = 0;

        if (!this.isConvertedToArray()) {
            this.convertToArrays();
        }
        if (!v.isConvertedToArray()) {
            v.convertToArrays();
        }
        int index1 = 0;
        int index2 = 0;
        int[] keys1 = this.getKeySetArray();
        int[] keys2 = v.getKeySetArray();
        int[] vals1 = this.getValueSet();
        int[] vals2 = v.getValueSet();

        for (double i : vals1) {
            length1 += Math.pow(i, 2);
        }
        length1 = Math.sqrt(length1);
        if (length1 == 0) {
            return 0.0;
        }

        double length2 = 0;
        for (double i : vals2) {
            length2 += Math.pow(i, 2);
        }
        length2 = Math.sqrt(length2);
        if (length2 == 0) {
            return 0.0;
        }

        while (index1 < keys1.length && index2 < keys2.length) {
            int comp = keys1[index1] - keys2[index2];
            if (comp == 0) {
                cosine += (vals1[index1++] * vals2[index2++]);
            } else if (comp < 0) {
                index1++;
            } else {
                index2++;
            }
        }

        return (cosine / (length1 * length2));
    }
    

    public double getInnerItSelf() {
        if (lengthCompuetd) {
            return this.lengthPower2;
        } else {

            double length2 = 0.0;
            if (!this.isConvertedToArray()) {
                this.convertToArrays();
            }

            int[] vals1 = this.getValueSet();

            for (double i : vals1) {
                length2 += Math.pow(i, 2);
            }
            this.lengthPower2 = length2;
            lengthCompuetd = true;
            return length2;
        }
    }

   

 
    
}
