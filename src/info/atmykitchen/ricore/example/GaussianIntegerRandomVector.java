/*
 * All rights reserved
 * Copyright Behrang Qasemizadeh
 */
package info.atmykitchen.ricore.example;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;


/**
 *
 * @author Behrang QasemiZadeh <me at atmykitchen.info>
 */
public class GaussianIntegerRandomVector {

    /**
     *
     * @param dimension is the diemnsion of vector
     * @param beta is the probability of the
     *
     */
    double beta;
    int reducedDimension;
    int originalDimension;
    int numberNonZeroElement;
    /**
     * @param DATA_SPARSITY shows the expected sparsity of the original vector
     * space; in text analytics, this value is very very very small; smaller
     * than 0.05
     */
    final double DATA_SPARSITY = 0.05;
    Random randomElement;
    Random randomValuePositive;
    Random randomValueNegative;

    public GaussianIntegerRandomVector(int reducedDimension, int directNumberOfNoneZeroElement) throws Exception {
        this.reducedDimension = reducedDimension;
        if (directNumberOfNoneZeroElement == 0) {
            throw new Exception("You have to provide the number of non-zero elements in the vectors;\nYou can also use the alternative constructor and provide the  original dimensionality");
        } else {
            this.numberNonZeroElement = directNumberOfNoneZeroElement;
            this.beta = 1 / (reducedDimension * 1.0 / directNumberOfNoneZeroElement);
        }
        System.out.println("Parameters m=" + reducedDimension + " beta=" + beta + " non-zero element: " + numberNonZeroElement);
        init();

    }

    /**
     * Several ways to instantiate
     *
     * @param originalDimension
     * @param reducedDimension
     * @param directNumberOfNoneZeroElement
     */
    public GaussianIntegerRandomVector(int originalDimension, int reducedDimension, int directNumberOfNoneZeroElement) {

        this.originalDimension = originalDimension;
        this.reducedDimension = reducedDimension;

        if (directNumberOfNoneZeroElement == 0) {
            this.beta = 1 / Math.sqrt(originalDimension * DATA_SPARSITY);
        } else {
            this.beta = 1 / (reducedDimension * 1.0 / directNumberOfNoneZeroElement);
        }
        System.out.println("Parameters are n=" + originalDimension + " m=" + reducedDimension + " beta=" + beta);
        init();

    }

    private void init() {
        randomElement = new Random();
        randomValuePositive = new Random();
        randomValueNegative = new Random();

        numberNonZeroElement = (int) (2 * Math.round(beta * reducedDimension / 2)); // get the closet 

    }

    /**
     * Get the next element of index vectors that will be non-zero randomly
     *
     * @return
     */
    private int getNextRandomIndexElement() {
        return randomElement.nextInt(reducedDimension);
    }

    /**
     * Main method to generate a random vector of Gaussian distribution as set
     * in the constructor
     *
     * @return
     */
    public SparseIntegerVector getNormalRandomVector() {
        SparseIntegerVector spd = new SparseIntegerVector(reducedDimension);
        Set<Integer> nonzeroElement = new HashSet<>();
        while (nonzeroElement.size() != numberNonZeroElement) {
            nonzeroElement.add(getNextRandomIndexElement());
        }
        Iterator<Integer> iterator = nonzeroElement.iterator();
        // set half to poisitve another half to negative
        // so many implementations for this bit; 

        boolean isPostiveTurn = true;
        while (iterator.hasNext()) {
            if (isPostiveTurn) {
                spd.put(iterator.next(), 1);
                isPostiveTurn = false;
            } else {
                spd.put(iterator.next(), -1);
                isPostiveTurn = true;
            }
        }
        return spd;
    }

    public String getInfo() {
        return "#\t data_sparsity_factor:" + this.DATA_SPARSITY + "\t non-zero elements:" + this.numberNonZeroElement + "\tbeta:" + this.beta;
    }

    public int getNumberNonZeroElement() {
        return numberNonZeroElement;
    }

   
//    public static void main(String[] args) throws Exception {
//        
//
//        GaussianIntegerRandomVector crv = new GaussianIntegerRandomVector(1800, 4);
//
//        System.out.println(crv.getNormalRandomVector().toString());
//        System.out.println(crv.getNormalRandomVector().toString());
//
//    }

}
