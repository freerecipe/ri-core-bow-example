/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.atmykitchen.ricore.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeMap;

/**
 *
 * @author Behrang QasemiZadeh <me at atmykitchen.info>
 */
public class CoreRandomIndexing {

    /**
     * Simple bag of word exmaple
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        int modelDimension = 1000; // the dimensionality of the model
        // random vector generation: random vectors of dimention 1000 and 2 non-zero elements
        GaussianIntegerRandomVector grv = new GaussianIntegerRandomVector(modelDimension, 2);
        // use a tree map to store all generated index vectors; of course you can use a database etc instead
        TreeMap<String, SparseIntegerVector> indexVectorMap = new TreeMap();

        // use a tree map to store all generated context vectors
        TreeMap<String, SparseIntegerVector> contextVectorMap = new TreeMap();

        // assume that input documents are in the folder input
        // for each document create its *context vector*
        File dir = new File("input");
        for (int i = 0; i < dir.listFiles().length; i++) {
            if (dir.listFiles()[i].isFile()) {
                if (dir.listFiles()[i].getName().endsWith("txt")) {
                    //for each text file (a) create context vector (b) update the context vector by adding index vectors to it 
                    String fileContent = readAllTheFile(dir.listFiles()[i]);
                   
                    SparseIntegerVector documentContextVector = new SparseIntegerVector(modelDimension);
                    String[] bagOfWords = fileContent.trim().split(" "); //find the words -- use any method to define index vectors ...this is a simple demo
                    for (String word : bagOfWords) {
                        String trimWord = word.trim();
                        if(indexVectorMap.containsKey(trimWord)){
                            // if there is an index vector for the word add it to the context vector
                            documentContextVector.addVector(indexVectorMap.get(trimWord));
                        }else{
                            // create a new ranodm index vector, assign it to the word and add it to the context vector
                            SparseIntegerVector normalRandomVector = grv.getNormalRandomVector();
                            indexVectorMap.put(trimWord, normalRandomVector);
                            documentContextVector.addVector(normalRandomVector);
                        }
                        
                    }
                    contextVectorMap.put(dir.listFiles()[i].getName(), documentContextVector);
                }
            }
        }
        // now you have all the index vectors and context vectors
        // store them for later usages, e.g., classification or for updatin them etc. all you have to do it to load them into the SparseIntegerVector
        // I have implemented a cosine meaasure that you can use
        
        // I write them into the folder output
        writeVectors(indexVectorMap, "output/index-vectors");
        writeVectors(contextVectorMap, "output/context-vectors");
        
        
        


    }
    /**
     * Use sparse vector representation to serialise the vectors into text file
     * @param theMap
     * @param fileName
     * @throws Exception 
     */
    private static void writeVectors(TreeMap<String, SparseIntegerVector> theMap, String fileName) throws Exception{
       PrintWriter  pw = new  PrintWriter (new FileWriter(fileName));
        for(String name: theMap.keySet()){
            pw.println(name+"\t" +theMap.get(name).toStringInteger());
            
        }
        pw.close();
    }
    
    private static String readAllTheFile(File f) throws Exception{
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }
}
